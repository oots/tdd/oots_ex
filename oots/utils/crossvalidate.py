
import gevent
from gevent.pywsgi import WSGIServer
from gevent import monkey
monkey.patch_all()

from flask import Flask, request
import sys, lxml.etree, datetime, traceback, uuid, tomllib, tempfile
import oots.utils.schematron, oots.utils.sslcontext

from inspect import getsourcefile
from os.path import abspath, dirname, join
thisdir = dirname(abspath(getsourcefile(lambda: 0)))


class ValidationService(Flask):
    def __init__(self, config_file):
        super().__init__(__name__)

        with open(config_file, 'rb') as fd:
            self.settings = tomllib.load(fd)

        fn = self.settings['rules']['schematron']
        with open(fn, 'rb') as fd:
            schtext = fd.read().decode('utf-8')
            self.sch_validator = oots.utils.schematron.SchematronValidator(
                xml_text=schtext
            )

        self.add_url_rule('/', view_func=self.do_home, methods=['GET'])
        self.add_url_rule('/validate', view_func=self.do_validate, methods=['POST'])

    def do_home(self):
        return (  self.settings['frontend']['html']
        )

    def do_validate(self):
        # check if the post request has the file part
        docs = lxml.etree.Element('Documents')
        for part in request.files:
            data = request.files[part].read()
            if len(data) > 10:
                sub = lxml.etree.SubElement(docs, 'Document')
                sub.set('name', part)
                part_tree = lxml.etree.fromstring(data)
                sub.append(part_tree)

        doccontent = lxml.etree.tostring(docs)
        print(doccontent)

        (count, report) = self.sch_validator.validate(xml_text=doccontent)
        return(
            """<html>
            <h1>Result of validation</h1>
            <p>Number of failed asserts:  {}</p>
            <pre>{}</pre>
            </html>
            """.format(count,
                       report,
                       )
        )

    def serve_forever(self):
        (keyfile, certfile) = oots.utils.sslcontext.generate_sslcontext()
        http_server = WSGIServer(('', self.settings['webserver']['server_port']),
                                 self, keyfile=keyfile, certfile=certfile)
        gevent.joinall([
            gevent.spawn(http_server.serve_forever),
        ])


if __name__ == "__main__":
    if len(sys.argv) > 1:
        conf = sys.argv[1]
    else:
        conf = 'config.toml'
    x = ValidationService(conf)
    x.serve_forever()


