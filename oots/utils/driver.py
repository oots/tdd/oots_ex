# Utility function to get the driver and SDG XSDs from the Internet.
# Returns the compiled driver.

import tempfile, requests, os, lxml

def load_driver(
        uri_prefix='https://code.europa.eu/oots/tdd/tdd_chapters/-/blob/master/OOTS-EDM/xsd/sdg/',
        driver_file='oots_driver_v1.0.0.xsd',
        sdg_file='SDG-GenericMetadataProfile-v1.0.0.xsd'):

    driver_src = uri_prefix + driver_file
    driver_content = requests.get(driver_src).text.encode('utf8')
    sdg_src = uri_prefix + sdg_file
    sdg_content = requests.get(sdg_src).text.encode('utf8')

    with tempfile.TemporaryDirectory() as tempdirname:

        driver_file = os.path.join(tempdirname, driver_file)
        f1 = open(driver_file, 'wb')
        f1.write(driver_content)
        f1.close()

        sdg_file = os.path.join(tempdirname, sdg_file)
        f2 = open(sdg_file, 'wb')
        f2.write(sdg_content)
        f2.close()

        cwd = os.getcwd()
        os.chdir(tempdirname)
        fd = open(driver_file, 'rb')
        xsdt = lxml.etree.parse(fd)
        driver = lxml.etree.XMLSchema(xsdt)
        os.chdir(cwd)

    return(driver)

