# Utility function to create an ECDSA private key and related self-signed
# X.509 certificate for use with the web server of the data service / preview
# space

import tempfile, os.path, uuid, datetime

from cryptography.hazmat.primitives import serialization
#from cryptography.hazmat.primitives.asymmetric import rsa
from cryptography.hazmat.primitives.asymmetric import ec

from cryptography import x509
from cryptography.x509.oid import NameOID
from cryptography.hazmat.primitives import hashes

def generate_sslcontext(country_name=u"BE", state_or_province_name=u"Brussels", locality_name=u"Brussels",
                        organization_name=u"OOTS", common_name=u"OOTS.example.com", dns_name=u"localhost"):
    unique_identifier = uuid.uuid4()
    keyfile = os.path.join(tempfile.gettempdir(), "{}.pem".format(unique_identifier))

    # Generate our key
    key = ec.generate_private_key(
        #public_exponent=65537,
        #key_size=2048,
        ec.SECP256R1()
    )
    # Write our key to disk for safe keeping
    with open(keyfile, "wb") as f:
        f.write(key.private_bytes(
            encoding=serialization.Encoding.PEM,
            format=serialization.PrivateFormat.TraditionalOpenSSL,
            encryption_algorithm=serialization.NoEncryption(),
        ))

    certfile = os.path.join(tempfile.gettempdir(), "{}.cert".format(unique_identifier))
    subject = issuer = x509.Name([
        x509.NameAttribute(NameOID.COUNTRY_NAME, country_name),
        x509.NameAttribute(NameOID.STATE_OR_PROVINCE_NAME, state_or_province_name),
        x509.NameAttribute(NameOID.LOCALITY_NAME, locality_name),
        x509.NameAttribute(NameOID.ORGANIZATION_NAME, organization_name),
        x509.NameAttribute(NameOID.COMMON_NAME, common_name),
    ])
    cert = x509.CertificateBuilder().subject_name(
        subject
    ).issuer_name(
        issuer
    ).public_key(
        key.public_key()
    ).serial_number(
        x509.random_serial_number()
    ).not_valid_before(
        datetime.datetime.now(datetime.timezone.utc)
    ).not_valid_after(
        # Our certificate will be valid for 10 days
        datetime.datetime.now(datetime.timezone.utc) + datetime.timedelta(days=10)
    ).add_extension(
        x509.SubjectAlternativeName([x509.DNSName(dns_name)]),
        critical=False,
        # Sign our certificate with our private key
    ).sign(key, hashes.SHA256())
    # Write our certificate out to disk.
    with open(certfile, "wb") as f:
        f.write(cert.public_bytes(serialization.Encoding.PEM))
    return(keyfile, certfile)






