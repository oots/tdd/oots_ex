import dns.resolver, re, sys
from dns.rdatatype import RdataType



def cs_lookup(name):
    r =  dns.resolver.Resolver()
    r.nameservers = ['8.8.8.8']
    rr = r.resolve(name, rdtype=RdataType.NAPTR)
    for r in rr:
        m = re.search('^\!\.\*\!(.*)\!$', r.regexp.decode('utf8'))
        if m:
            return(m[1])

if __name__ == "__main__":
    if len(sys.argv) == 2:
        # For example:    be.dsd.v1.cs.acc.oots.tech.ec.europa.eu
        name = sys.argv[1]
        print(cs_lookup(name))
    else:
        print("Usage:  python cs_discovery.py  <NAME>")



