
from inspect import getsourcefile
from os.path import abspath, dirname, join
thisdir = dirname(abspath(getsourcefile(lambda: 0)))
schxslt = join(thisdir,'schxslt-1.9.5/2.0/pipeline-for-svrl.xsl')


from  saxonche  import PySaxonProcessor, PyXslt30Processor

import requests

class SchematronValidator():

    def __init__(self, xml_file_name=None, xml_uri=None, xml_text=None):
        self.sch_validator = None
        self.proc = PySaxonProcessor(license=False)

        # First XSL compiler is used to compile SCH into XSLT
        xslt_processor = self.proc.new_xslt30_processor()
        sch_compiler = xslt_processor.compile_stylesheet(
            stylesheet_file=schxslt
        )

        if xml_uri is not None:
            result = requests.get(xml_uri)
            if result.status_code != 200:
                raise Exception("Can't download from {}: {}".format(xml_uri, result.status_code))
            else:
                sch_source = self.proc.parse_xml(xml_text=result.content.decode('utf8'))
        elif xml_file_name is not None:
            sch_source = self.proc.parse_xml(xml_file_name=xml_file_name)
        elif xml_text is not None:
            sch_source = self.proc.parse_xml(xml_text=xml_text)
        else:
            raise Exception('Call with one of xml_uri, xml_file_name or xml_text keyword parameters')

        sch_compiler.set_initial_match_selection(xdm_value=sch_source)
        compiled_sch_text = sch_compiler.apply_templates_returning_string()
        self.compiled_sch_text = compiled_sch_text

        # Second XSL stylesheet is used to validate XML against the source XML file
        sch_validator = self.proc.new_xslt30_processor()
        self.transformer = sch_validator.compile_stylesheet(
            stylesheet_text=compiled_sch_text)

        # Third XSL stylesheet is use to filter the SVRL output
        svrl_filter_src = """<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
    xmlns:sch="http://purl.oclc.org/dsdl/schematron"
    xmlns:error="https://doi.org/10.5281/zenodo.1495494#error"
    xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
    xmlns:schxslt-api="https://doi.org/10.5281/zenodo.1495494#api"
    xmlns:schxslt="https://doi.org/10.5281/zenodo.1495494"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:sdg="http://data.europa.eu/p4s"
    xmlns:rs="urn:oasis:names:tc:ebxml-regrep:xsd:rs:4.0"
    xmlns:rim="urn:oasis:names:tc:ebxml-regrep:xsd:rim:4.0"
    xmlns:query="urn:oasis:names:tc:ebxml-regrep:xsd:query:4.0"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xmlns:xlink="http://www.w3.org/1999/xlink"
    exclude-result-prefixes="xs error rdf schxslt-api schxslt"
    version="2.0">
    
    <xsl:output indent="yes"/>

    <xsl:template  match="svrl:schematron-output">
        <svrl:schematron-output>
            <xsl:apply-templates  select="child::svrl:failed-assert[not(@role) or @role = 'fatal' 
                or @role = 'FATAL' or @role='error' or @role='ERROR']"/>
        </svrl:schematron-output>
    </xsl:template>
    
    <xsl:template match="svrl:failed-assert">
        <xsl:copy-of select="current()" />
    </xsl:template>
    
</xsl:stylesheet>
        """


        self.svrl_filter = xslt_processor.compile_stylesheet(
            stylesheet_text=svrl_filter_src
        )

        # Fourth XSL stylesheet is just to count the number of fatal/error failed asserts
        failed_assert_counter_src = """<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
    version="2.0">
    <xsl:output  media-type="xml" omit-xml-declaration="yes"/>
    <xsl:template  match="svrl:schematron-output">
        <xsl:value-of select="count(child::svrl:failed-assert[not(@role) or @role = 'fatal' 
            or @role = 'FATAL' or @role='error' or @role='ERROR'])"/>     
    </xsl:template>
</xsl:stylesheet>"""
        self.failed_assert_counter = xslt_processor.compile_stylesheet(
            stylesheet_text=failed_assert_counter_src
        )

    def __del__(self):
        del(self.proc)

    def as_xslt(self):
        return self.compiled_sch_text.encode('utf8')

    def isvalid(self, xml_text=None,xml_file_name=None):
        (total, report) = self._validate(xml_text, xml_file_name)
        if int(float(total)) > 0:
            return False
        else:
            return True

    def  validate(self, xml_text=None,xml_file_name=None):
        return(self._validate(xml_text, xml_file_name))

    def _validate(self, xml_text=None,xml_file_name=None):
        if xml_file_name is not None:
            source = self.proc.parse_xml(xml_file_name=xml_file_name)
        elif xml_text is not None:
            source = self.proc.parse_xml(xml_text=xml_text.decode('utf8'))
        else:
            raise Exception('Call with one of xml_uri, xml_file_name or xml_text keyword parameters')
        self.transformer.set_initial_match_selection(xdm_value=source)
        svrl = self.transformer.apply_templates_returning_string()

        svrl_filter_source = self.proc.parse_xml(xml_text=svrl)
        self.svrl_filter.set_initial_match_selection(xdm_value=svrl_filter_source)
        result_1 = self.svrl_filter.apply_templates_returning_string()
        failed_assert_counter_source = self.proc.parse_xml(xml_text=svrl)
        self.failed_assert_counter.set_initial_match_selection(xdm_value=failed_assert_counter_source)
        result_2 =  self.failed_assert_counter.apply_templates_returning_string()
        return(int(result_2), result_1)


