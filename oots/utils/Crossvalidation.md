# Cross artifact validation

## 1. Introduction 

In OOTS, we already use XSD and Schematron to express and  validate
various XML artifacts used by the OOTS Common
Services and evidence exchange. However, there are also many constraints 
that cross the boundaries of a single artifact. For example
   * a request and its response must have matching request indentifiers.
   * a sequences of AS4 messages must have the same *ConversationIdentifier*.
   * the *Action* of the ebMS header has impact on the content of the payload.

The *crossvalidate.py* module provides a simple way to validate
constraints between various artifacts. The constraints can
be expressed as regular Schematron rules where the 
rules refer to a context wrapper.  A simple Web form server allows
users to upload various artifacts individually. The server
does the wrapping and applies the Schematron rules.


## 2. Cross Artifact Schematron rules

A simple way to express constraints between artifacts
is to wrap them in a canonical wrapper and define
rules that link content in the wrapped parts.  

The tooling assumes the following structure:

```xml 
<Documents>
  <Document name="part_a">...</Document>
  <Document name="part_b">...</Document>
 <!-- possibly other parts can follow -->
</Documents>
```
Here the *name* labels can vary depend on what 
you want to validate.  You can just test two types
of artifacts or many. 

The following example defines some constraints
between the ebMS3 headers for four preview messages.
The must all have the same *ConversationId*. Also,
the *originalSender* in the first request must be 
the same as the *finalRecipient* in the first 
response.

```xml
<?xml version="1.0" encoding="UTF-8"?>
<sch:schema xmlns:sch="http://purl.oclc.org/dsdl/schematron" queryBinding="xslt2"
    xmlns:sqf="http://www.schematron-quickfix.com/validator/process">
    
    <sch:ns uri="http://docs.oasis-open.org/ebxml-msg/ebms/v3.0/ns/core/200704/" prefix="eb"/>
    
    <sch:pattern>
        <sch:rule context="//Documents">
            <sch:let name="conv1" value="child::Document[@name='req1']//eb:ConversationId"/>
            <sch:let name="conv2" value="child::Document[@name='resp1']//eb:ConversationId" />
            <sch:let name="conv3" value="child::Document[@name='req2']//eb:ConversationId" />
            <sch:let name="conv4" value="child::Document[@name='resp2']//eb:ConversationId" />
            <sch:assert test=" $conv1=$conv2"
                >Request 1 and response 2 have different values for ConversationId <sch:value-of 
                    select="$conv1"/> and <sch:value-of select="$conv2"/></sch:assert>
            <sch:assert test=" $conv1=$conv3"
                >Request 1 and response 3 have different values for ConversationId <sch:value-of 
                    select="$conv1"/> and <sch:value-of select="$conv3"/></sch:assert>
            <sch:assert test=" $conv1=$conv4"
                >Request 1 and response 4 have different values for ConversationId <sch:value-of 
                    select="$conv1"/> and <sch:value-of select="$conv4"/></sch:assert>

            <sch:let name="os1" value="child::Document[@name='req1']//eb:Property[@name='originalSender']/text()" />
            <sch:let name="fr1" value="child::Document[@name='req1']//eb:Property[@name='finalRecipient']/text()" />
            <sch:let name="os2" value="child::Document[@name='resp1']//eb:Property[@name='originalSender']/text()" />
            <sch:let name="fr2" value="child::Document[@name='resp1']//eb:Property[@name='finalRecipient']/text()" />


            <sch:assert test=" $os1=$fr2"
                >Request 1 and response 2 have different values for C1 - C4 <sch:value-of 
                    select="$os1"/> and <sch:value-of select="$fr2"/></sch:assert>
            
            <sch:assert test=" $fr1=$os2"
                >Request 1 and response 2 have different values for C4 / C1 <sch:value-of 
                    select="$fr1"/> and <sch:value-of select="$os2"/></sch:assert>

        </sch:rule>
    </sch:pattern>
</sch:schema>
```


## 3. Web server module

The module *crossvalidate.py* allows different 
artificats to be submitted using HTML form posts. 
It wraps the various parts, applies cross-artifact 
validation rules and returns the results. 

The module can be started from the command line using 
a configuration file as parameter.  

```commandline
python crossvalidate.py config.toml
```

This command starts a server on the (local) host.

The parameter is optional. If not specified, 
the script looks for a file called 
*config.toml* in the working directory.

Note that the server uses a self-signed
TLS server certificate.  You need to click
through browser warnings.

The logic of the script is in the TOML file.

   * The *schematron* parameter in *rules* has the name of a Schematron file to be used as rules.
   * The *server_port* in *webserver* is the port used by the application.
   * *frontend* section has the front page. It can be configured for different types of artifacts.

 The names in the *frontend* must match the label in Schematron.

```toml
[rules]
schematron = 'cross_validate.sch'

[webserver]
# We are running an HTTPS server on this port
server_port = 5000

[frontend]

html = """
"<html>
<h1>Validate OOTS artifacts</h1> \
<form method=post enctype=multipart/form-data action='validate'> \
1 ebMS header request 1 <input type=file name=req1><br> \
2 ebMS header response 1 <input type=file name=resp1><br> \
3 ebMS header request 2 <input type=file name=req2><br> \
4 ebMS header response 2 <input type=file name=resp2><br> \
<input type=submit value=Validated> \
</form> \
</html>\
"""


```

With these settings, the server will start on port 5000 on localhost and display 
the following form.

![Front end](Images/frontend.jpg "frontend")

From the browser the user can upload individual XML parts.

By pressing *validate*, the user can request the set to be
validated using the configured rules. The result is
now a simple listing of the Schematron messages.

![Front end](Images/result.jpg)

## 4. Installation

The script is a module in the *utils* section
of [the *oots_ex* repository](https://code.europa.eu/oots/tdd/oots_ex):

