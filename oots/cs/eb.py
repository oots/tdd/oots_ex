
import os, logging, requests, lxml.etree
from urllib.parse import urljoin
from oots.cs.cs_discovery import cs_lookup
from oots.cs.cs_common import add_url_suffix


def get_list_of_requirements(
        country_code, procedure_id=None,  environment='prod'
):
    name = '{}.{}.v1.cs.{}.oots.tech.ec.europa.eu'.format(country_code, 'eb', environment).lower()
    server = cs_lookup(name)
    query = add_url_suffix(server, 'rest/search?')
    paramd = {}
    for (p,v) in [
        ('queryId', 'urn:fdc:oots:eb:ebxml-regrep:queries:requirements-by-procedure-and-jurisdiction'),
        ('procedure-id', procedure_id),
        ('country-code', country_code.upper())
    ]:
        if v is not None:
            paramd[p] = v

    res = requests.get(query,params=paramd)
    #print("Query: {} result:\n{}".format(res.url, res.text))
    return lxml.etree.fromstring(res.text.encode('utf8'))

def get_evidence_types(
        country_code, requirement_id, environment='prod'
):
    name = '{}.{}.v1.cs.{}.oots.tech.ec.europa.eu'.format(country_code, 'eb', environment).lower()
    server = cs_lookup(name)
    query = add_url_suffix(server, 'rest/search?')
    paramd = {}
    for (p,v) in [
        ('queryId', 'urn:fdc:oots:eb:ebxml-regrep:queries:evidence-types-by-requirement-and-jurisdiction'),
        ('requirement-id', requirement_id),
        ('country-code', country_code.upper())
    ]:
        if v is not None:
            paramd[p] = v
    res = requests.get(query,params=paramd)
    #print("Query: {} result:\n{}".format(res.url, res.text))
    return lxml.etree.fromstring(res.text.encode('utf8'))
