
import lxml.etree

def add_url_suffix(prefix, suffix):
    if prefix[-1] == '/':
        return "{}{}".format(prefix, suffix)
    else:
        return "{}/{}".format(prefix, suffix)

def is_exception(response):
    exception_l = response.xpath(
        '//rs:Exception',
        namespaces={
            'rs':"urn:oasis:names:tc:ebxml-regrep:xsd:rs:4.0"
        }
    )
    if len(exception_l) > 0:
        exception = response.xpath(
            '//rs:Exception',
            namespaces={
                'rs': "urn:oasis:names:tc:ebxml-regrep:xsd:rs:4.0"
            }
        )[0]
        return (True,
                {
                    'message': exception.get('message'),
                    'code': exception.get('code'),
                    'severity': exception.get('severity')
                }
        )
    else:
        return (False, None)

