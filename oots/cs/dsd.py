import os, requests, lxml.etree, logging
from oots.cs.cs_discovery import cs_lookup
from oots.cs.cs_common import add_url_suffix

def get_data_services(
        country_code, classification,  environment='prod'
):
    name = '{}.{}.v1.cs.{}.oots.tech.ec.europa.eu'.format(country_code, 'dsd', environment).lower()
    server = cs_lookup(name)
    query = add_url_suffix(server, 'rest/search?')
    paramd = {}
    for (p,v) in [
        ('queryId', 'urn:fdc:oots:dsd:ebxml-regrep:queries:dataservices-by-evidencetype-and-jurisdiction'),
        ('evidence-type-classification', classification),
        ('country-code', country_code.upper())
    ]:
        if v is not None:
            paramd[p] = v

    res = requests.get(query,params=paramd)
    logging.debug("Data service query for {} in {}:\n{}".format(
        classification, country_code,  res.text
    ))
    return lxml.etree.fromstring(res.text.encode('utf8'))
