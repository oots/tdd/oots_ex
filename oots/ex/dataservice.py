"""
Dataservice client and server functionality


Limitations:
-  no support for IP agents, only ER and EP.
-  content for Agent is minimalist (so no Address etc.)
-  no preview
"""


import gevent
from gevent.pywsgi import WSGIServer
from gevent import monkey
monkey.patch_all()

import requests

from flask import Flask
import sys, lxml.etree, datetime, traceback, uuid, tomllib, tempfile
import oots.ex.domibus_ws, oots.utils.schematron, oots.utils.sslcontext, oots.utils.driver

# Namespace definitions and utility functions
NSMAP = {
    'query': 'urn:oasis:names:tc:ebxml-regrep:xsd:query:4.0',
    'rs': 'urn:oasis:names:tc:ebxml-regrep:xsd:rs:4.0',
    'rim': 'urn:oasis:names:tc:ebxml-regrep:xsd:rim:4.0',
    'xsi': 'http://www.w3.org/2001/XMLSchema-instance',
    'xml': 'http://www.w3.org/XML/1998/namespace',
    'sdg': 'http://data.europa.eu/p4s',
    'xlink': 'http://www.w3.org/1999/xlink'
}

def query_ns(el):
    return '{{{}}}{}'.format(NSMAP['query'], el)

def rs_ns(el):
    return '{{{}}}{}'.format(NSMAP['rs'], el)

def rim_ns(el):
    return '{{{}}}{}'.format(NSMAP['rim'], el)

def xsi_ns(el):
    return '{{{}}}{}'.format(NSMAP['xsi'], el)

def xml_ns(el):
    return '{{{}}}{}'.format(NSMAP['xml'], el)

def sdg_ns(el):
    return '{{{}}}{}'.format(NSMAP['sdg'], el)

def xlink_ns(el):
    return '{{{}}}{}'.format(NSMAP['xlink'], el)

def attach_simple_slot(parent_el, name=None, type=None, value=None):
    slot_el = lxml.etree.SubElement(
        parent_el, rim_ns('Slot'), name=name
    )
    slot_value_el = lxml.etree.SubElement(
        slot_el, rim_ns('SlotValue')
    )
    slot_value_el.set(
        xsi_ns("type"), type
    )
    slot_value =  lxml.etree.SubElement(slot_value_el, rim_ns("Value"))
    slot_value.text = value

def attach_evidence_requester(parent_el, partyid, partyidtype=None, collection=True, agent_classification='ER'):
    _attach_exchange_party(parent_el, partyid, partyidtype=partyidtype, exchange_role="EvidenceRequester",
                           agent_classification=agent_classification, collection=collection)

def attach_evidence_provider(parent_el, partyid, partyidtype=None, collection=False, agent_classification=None):
    _attach_exchange_party(parent_el, partyid, partyidtype=partyidtype, exchange_role="EvidenceProvider",
                           agent_classification=agent_classification, collection=collection)

def attach_error_provider(parent_el, partyid, partyidtype=None, request_message=None, agent_classification='ERRP'):
    # either partyid and partyidtype are set, or we use theC4 in the request
    if partyid is None:
        partyid = request_message.c4_party_id
        if request_message.c4_party_id is not None:
            partyidtype = request_message.c4_party_id
    _attach_exchange_party(parent_el, partyid, partyidtype=partyidtype, exchange_role="ErrorProvider",
                           collection=False, agent_classification=agent_classification)

def _attach_exchange_party(parent_el, partyid, partyidtype=None,
                           exchange_role="EvidenceRequester", agent_classification=None, collection=True):
    exchange_party_el = lxml.etree.SubElement(
        parent_el, rim_ns('Slot'), name=exchange_role
    )
    exchange_party_value_el = lxml.etree.SubElement(
        exchange_party_el,
        rim_ns('SlotValue')
    )
    if collection:
        exchange_party_value_el.set(
            xsi_ns("type"), "rim:CollectionValueType"
        )
        exchange_party_element_el = lxml.etree.SubElement(
            exchange_party_value_el,
            rim_ns('Element')
        )
        exchange_party_element_el.set(
            xsi_ns('type'), 'rim:AnyValueType'
        )
        exchange_agent_el = lxml.etree.SubElement(
            exchange_party_element_el,
            sdg_ns('Agent')
        )
    else:
        exchange_party_value_el.set(
            xsi_ns("type"), "rim:AnyValueType"
        )
        exchange_agent_el = lxml.etree.SubElement(
            exchange_party_value_el,
            sdg_ns('Agent')
        )
    partyid_el = lxml.etree.SubElement(
        exchange_agent_el,
        sdg_ns('Identifier')
    )
    name_el = lxml.etree.SubElement(
        exchange_agent_el,
        sdg_ns('Name')
    )
    if collection:
        name_el.text = "Name of {}".format(partyid)
    if agent_classification is not None:
        classification_el = lxml.etree.SubElement(
            exchange_agent_el,
            sdg_ns('Classification')
        )
        classification_el.text = agent_classification
    partyid_el.text = partyid
    if partyidtype is not None:
        partyid_el.set('schemeID', partyidtype)



"""
DS Client
"""
class DSClient():

    def __init__(self, accesspoint_url, handler=None, party_id=None, party_id_type=None):
        self.c2_party_id = party_id
        self.c2_party_id_type = party_id_type
        self.accesspoint_url = accesspoint_url
        self.natural_person_query_handler = handler

    def ds_submit_request(self, payload, requesterid, providerid, accessserviceid,
                          requesteridtype=None, provideridtype=None, accessserviceidtype=None):
        message = oots.ex.domibus_ws.eDeliveryMessage(
            c1_party_id=requesterid,
            c1_party_id_type=requesteridtype,
            c2_party_id=self.c2_party_id,
            c2_party_id_type=self.c2_party_id_type,
            c3_party_id=accessserviceid,
            c3_party_id_type=accessserviceidtype,
            c4_party_id=providerid,
            c4_party_id_type=provideridtype,
            service='QueryManager',
            serviceType='urn:oasis:names:tc:ebcore:ebrs:ebms:binding:1.0',
            action='ExecuteQueryRequest'
        )
        message.attach("{}@er.oots.eu".format(uuid.uuid4()), 'application/x-ebrs+xml', payload)
        result = oots.ex.domibus_ws.submit(
            message,
            self.accesspoint_url
        )
        return result

    def ds_query(self, requesterid, providerid, accessserviceid,
                 procedureid, requirementid,
                 evidencetypeid,
                 evidencetypeclassification,
                 format='application/xml',
                 requesteridtype=None, provideridtype=None,
                 accessserviceidtype=None,
                 possibilityforpreview='false', explicitlyrequested='true',
                 naturalperson={}, legalperson={}, authorizedrepresentative={}):

        if naturalperson == {} and legalperson == {}:
            raise Exception('Either naturalperson or legalperson required in ds_query')

        if naturalperson != {}:
            for att in ['given_name', 'family_name', 'date_of_birth', 'loa']:
                if att not in naturalperson:
                    raise Exception('Attribute {} is required in naturalperson'.format(att))
            for att in naturalperson:
                if att not in ['given_name', 'family_name', 'date_of_birth', 'identifier',
                    'identifiertype', 'loa', 'place_of_birth', 'gender']:
                    print('Attribute {} is unknown, assumed to be sectoral')
            if 'identifier' in naturalperson and 'identifiertype' not in naturalperson:
                naturalperson['identifiertype'] = 'eidas'
        if legalperson != {}:
            for att in ['personid', 'personidtype',  'personname', 'loa']:
                if att not in legalperson:
                    raise Exception('Attribute {} is required in legalperson'.format(att))
            for att in legalperson:
                if att not in ['personid', 'personidtype',  'identifier','personname', 'loa']:
                    raise Exception('Legal person attribute {} unknown'.format(att))

        # First we generate a unique identifier for the query.  This identifier is returned to the
        # application and included it the request.  The data service server will include this identifier
        # in the responses it sends. This allows the client to correlate responses to requests.
        correlationid =  "urn:uuid:{}".format(uuid.uuid4())
        request_el = lxml.etree.Element(
            query_ns('QueryRequest'), id=correlationid, nsmap=NSMAP
        )

        #Specification identifier slot
        attach_simple_slot(
            request_el, 'SpecificationIdentifier', 'rim:StringValueType', 'oots-edm:v1.0'
        )

        #Issue DateTime slot
        attach_simple_slot(
            request_el, 'IssueDateTime', 'rim:DateTimeValueType', datetime.datetime.now().isoformat()
        )

        #Evidence request reason: Procedure
        procedure_el = lxml.etree.SubElement(
            request_el, rim_ns('Slot'), name="Procedure"
        )
        procedure_slotvalue_el = lxml.etree.SubElement(
            procedure_el, rim_ns('SlotValue')
        )
        procedure_slotvalue_el.set(
            xsi_ns("type"), "rim:InternationalStringValueType"
        )
        procedure_value_el = lxml.etree.SubElement(
            procedure_slotvalue_el, rim_ns('Value')
        )
        procedure_localized_value_el = lxml.etree.SubElement(
            procedure_value_el, rim_ns('LocalizedString')
        )
        procedure_localized_value_el.set(xml_ns('lang'),'EN')
        procedure_localized_value_el.set('value',procedureid)

        #Possibility for preview
        attach_simple_slot(
            request_el, 'PossibilityForPreview', 'rim:BooleanValueType', possibilityforpreview
        )

        #Explicit request given slot
        attach_simple_slot(
            request_el, 'ExplicitRequestGiven', 'rim:BooleanValueType', explicitlyrequested
        )

        #Evidence request reason: Requirement
        requirements_slot_el = lxml.etree.SubElement(
            request_el, rim_ns('Slot'), name="Requirements"
        )
        requirement_slotvalue_el = lxml.etree.SubElement(
            requirements_slot_el, rim_ns('SlotValue'),
            collectionType="urn:oasis:names:tc:ebxml-regrep:CollectionType:Set"
        )
        requirement_slotvalue_el.set(xsi_ns('type'), "rim:CollectionValueType")
        requirement_element_el = lxml.etree.SubElement(
            requirement_slotvalue_el, rim_ns('Element')
        )
        requirement_element_el.set(
            xsi_ns('type'), 'rim:AnyValueType'
        )
        requirement_el = lxml.etree.SubElement(requirement_element_el, sdg_ns('Requirement'))
        requirement_id_el = lxml.etree.SubElement(
            requirement_el, sdg_ns('Identifier')
        )
        requirement_id_el.text =  requirementid
        requirement_name_el = lxml.etree.SubElement(
            requirement_el, sdg_ns('Name'), lang='EN'
        )
        requirement_name_el.text = requirementid

        #Evidence requester
        attach_evidence_requester(request_el, requesterid, requesteridtype)

        #Evidence provider
        attach_evidence_provider(request_el, providerid, provideridtype)

        option_el = lxml.etree.SubElement(
            request_el, query_ns('ResponseOption'), returnType="LeafClassWithRepositoryItem"
        )

        query_el = lxml.etree.SubElement(
            request_el, query_ns('Query'), queryDefinition="DocumentQuery"
        )

        evidence_request_slot_el = lxml.etree.SubElement(
            query_el, rim_ns('Slot'), name="EvidenceRequest"
        )
        evidence_type_value_el = lxml.etree.SubElement(
            evidence_request_slot_el,
            rim_ns('SlotValue')
        )
        evidence_type_value_el.set(
            xsi_ns('type'), 'rim:AnyValueType'
        )
        ds_evidencetype_el = lxml.etree.SubElement(
            evidence_type_value_el,
            sdg_ns('DataServiceEvidenceType')
        )
        ds_evidencetype_id_el = lxml.etree.SubElement(
            ds_evidencetype_el,
            sdg_ns('Identifier')
        )
        ds_evidencetype_id_el.text = evidencetypeid
        ds_evidencetype_class_el = lxml.etree.SubElement(
            ds_evidencetype_el,
            sdg_ns('EvidenceTypeClassification')
        )
        ds_evidencetype_class_el.text=evidencetypeclassification

        dset_title = lxml.etree.SubElement(
            ds_evidencetype_el,
            sdg_ns('Title'), lang='EN'
        )
        dset_title.text = 'Title of the data service evidence type'
        distribution_el = lxml.etree.SubElement(
            ds_evidencetype_el, sdg_ns('DistributedAs')
        )
        format_el = lxml.etree.SubElement(
            distribution_el,
            sdg_ns('Format')
        )
        format_el.text = format

        if naturalperson is not None:
            natural_person_slot_el = lxml.etree.SubElement(
                query_el, rim_ns('Slot'), name="NaturalPerson"
            )
            natural_person_slot_value_el = lxml.etree.SubElement(
                natural_person_slot_el,
                rim_ns('SlotValue')
            )
            natural_person_slot_value_el.set(
                xsi_ns("type"), "rim:AnyValueType"
            )
            natural_person_el = lxml.etree.SubElement(
                natural_person_slot_value_el, sdg_ns('Person')
            )
            loa_el = lxml.etree.SubElement(
                natural_person_el, sdg_ns('LevelOfAssurance')
            )
            loa_el.text = naturalperson['loa']
            if 'identifier' in naturalperson:
                identifier_el = lxml.etree.SubElement(
                    natural_person_el, sdg_ns('Identifier'), schemeID= naturalperson['identifiertype']
                )
                identifier_el.text = naturalperson['identifier']
            fn_el = lxml.etree.SubElement(
                natural_person_el, sdg_ns('FamilyName')
            )
            fn_el.text = naturalperson['family_name']
            gn_el = lxml.etree.SubElement(
                natural_person_el, sdg_ns('GivenName')
            )
            gn_el.text = naturalperson['given_name']
            dob_el = lxml.etree.SubElement(
                natural_person_el, sdg_ns('DateOfBirth')
            )
            dob_el.text = naturalperson['date_of_birth']

        payload=lxml.etree.tostring(request_el)

        print(payload)
        result = self.ds_submit_request(payload, requesterid, providerid, accessserviceid, requesteridtype=requesteridtype,
                                   provideridtype=provideridtype, accessserviceidtype=accessserviceidtype)
        if result.status_code == 200:
            return correlationid
        else:
            raise Exception('Error submitting evidence request to Access Point: {}'.format(
                result.text
            ))

    def ds_handle_response(self, routing_table, messageid, message):
        """
        Function to handle responses to previously sent OOTS evidence requests
        :param routing_table: a dictionary of handlers indexed on the RegRep query identifier
        :param messageid: the identifier of the eDelivery response message (for logging etc.)
        :param message: the full eDelivery response message
        :return: None

        If a handler is found, it will be called with the RegRep correlation identifier, the message identifier
        and the RegRep payload as parameters.
        """
        (cid, contenttype, payload) = message.get_payload_by_position(0)
        regrep_response = lxml.etree.fromstring(payload)
        correlationid = regrep_response.get('requestId')
        print('Processing message {}'.format(messageid))
        if correlationid in routing_table:
            handler = routing_table[correlationid]
            handler(correlationid, messageid, message.soapenvelope)
            del(routing_table[correlationid])
        else:
            print('No handler defined for {} (message {})'.format(
                correlationid, messageid
            ))

"""
DS Server:  Data Service server module for OOTS

An OOTS data service has three layers
1.  eDelivery layer:  interface to an Access Point
2.  ebRS layer: package the service data using RegRep4
3.  OOTS application layer:  SDG data model and operations

This class uses Flask in preparation for the addition of Preview.
"""

class DSServer(Flask):
    def __init__(self, config_file='dataservice.toml'):

        # Load the server configuration
        with open(config_file, 'rb') as fd:
            self.settings = tomllib.load(fd)

        super().__init__(__name__)
        self.add_url_rule('/status', view_func=self.do_status, methods=['GET'])

        self.natural_person_query_handler = None

        #self.error_party_id=self.settings['accesspoint']['error_party_id']
        #self.error_party_id_type=self.settings['accesspoint']['error_party_id_type']

        self.driver = oots.utils.driver.load_driver(
            self.settings['xsd']['oots_xsd_uri'],
            self.settings['xsd']['oots_driver_xsd'],
            self.settings['xsd']['sdg_xsd'],
        )

        self.edm_req_c = oots.utils.schematron.SchematronValidator(
            xml_uri=self.settings['schematron']['edm_req_c_uri']
        )
        self.edm_req_s = oots.utils.schematron.SchematronValidator(
            xml_uri=self.settings['schematron']['edm_rec_s_uri']
        )
        self.edm_err_c = oots.utils.schematron.SchematronValidator(
            xml_uri=self.settings['schematron']['edm_err_c_uri']
        )
        self.edm_err_s = oots.utils.schematron.SchematronValidator(
            xml_uri=self.settings['schematron']['edm_err_s_uri']
        )
        self.edm_resp_c = oots.utils.schematron.SchematronValidator(
            xml_uri=self.settings['schematron']['edm_resp_c_uri']
        )
        self.edm_resp_s = oots.utils.schematron.SchematronValidator(
            xml_uri=self.settings['schematron']['edm_resp_s_uri']
        )


    def do_status(self):
        v = uuid.uuid4()
        print('Status: {}'.format(v))
        return ('Status: {}'.format(v), 200, {'mimetype': 'application/xml'})

    # eDelivery layer:  process eDelivery request and construct the eDelivery resppnse
    def handle_edelivery_request(self, messageid, edelivery_request):
        print("Handling evidence request {}:\n".format(
            messageid
            # ,lxml.etree.tostring(edelivery_request.soapenvelope)
        ), flush=True)


        response_payload_parts = []

        # eDelivery responses are reverse-routed:
        # - Reverse path is constructed from the request: c1->c4, c2->c3, c3->c2, c4->c1
        # - Specification reference: https://ec.europa.eu/cefdigital/wiki/display/SDGOO/eDelivery+Configuration
        # NB:  the action is just a default, it will be switched to ExceptionResponse if an exception occurs
        # while processing
        # TODO: the c1_party_id should be set to the error provider instead of the request c4 party.
        edelivery_response = oots.ex.domibus_ws.eDeliveryMessage(
            service='QueryManager',
            serviceType='urn:oasis:names:tc:ebcore:ebrs:ebms:binding:1.0',
            action='ExecuteQueryResponse',
            conversationid=edelivery_request.conversationid,
            c1_party_id=edelivery_request.c4_party_id,
            c1_party_id_type=edelivery_request.c4_party_id_type,
            c2_party_id=edelivery_request.c3_party_id,
            c2_party_id_type=edelivery_request.c3_party_id_type,
            c3_party_id=edelivery_request.c2_party_id,
            c3_party_id_type=edelivery_request.c2_party_id_type,
            c4_party_id=edelivery_request.c1_party_id,
            c4_party_id_type=edelivery_request.c1_party_id_type
        )

        # The request message has one payload part, which is a RegRep4 QueryRequest document
        # We fetch it from the eDelivery envelope,in which it is the first payload
        (contentid, mimetype, requestcontent) = edelivery_request.get_payload_by_position(0)

        # We know it's an XML document, so we try to parse it from its string serialisation
        # TODO: check that the C1 and C4 in the eDelivery envelope match the corresponding slots.
        try:
            regrep_request_el = lxml.etree.fromstring(requestcontent)
        except Exception:
            exception = traceback.format_exc()
            response_el = self.init_regrep_response(messageid, None)
            attach_error_provider(
                response_el,
                partyid=self.settings['accesspoint']['error_party_id'],
                partyidtype=self['accesspoint']['error_party_id_type'],
                request_message=None)
            response_el.append( self.create_exception(
                messageid, None,
                message='Syntactically or semantically invalid request',
                code='EDM:ERR:0003', type='rs:InvalidRequestExceptionType',
                detail='Request message is not well-formed: {}'.format(exception)))
            edelivery_response.action = 'ExceptionResponse'
            response_el.set('status', 'urn:oasis:names:tc:ebxml-regrep:ResponseStatusType:Failure')
        else:
            # The request is well-formed.  Check that it's valid too
            print('Request is well-formed: \n{}'.format(lxml.etree.tostring(regrep_request_el)))

            (validation_failures_count, validation_report) = self.validate_query_request(
                regrep_request_el, requestcontent
            )
            response_el = self.init_regrep_response(
                messageid, regrep_request_el
            )

            if validation_failures_count > 0:
                print('Validation errors ')
                attach_error_provider(
                    response_el, partyid=self.error_party_id, partyidtype=self.error_party_id_type,
                    request_message=None)
                response_el.append(self.create_exception(
                    messageid, regrep_request_el,
                    message='Syntactically or semantically invalid request',
                    code='EDM:ERR:0003', type='rs:InvalidRequestExceptionType',
                    detail=validation_report))
                edelivery_response.action = 'ExceptionResponse'
                response_el.set('status', 'urn:oasis:names:tc:ebxml-regrep:ResponseStatusType:Failure')
            else:
                print('No validation errors ')
                response_el.set('status', 'urn:oasis:names:tc:ebxml-regrep:ResponseStatusType:Success')
                requested_evidence_type = self.get_requested_evidence_type(regrep_request_el)

                provider_id_el = regrep_request_el.xpath(
                    '//rim:Slot[@name="EvidenceProvider"]//sdg:Agent/sdg:Identifier',
                    namespaces=NSMAP
                )[0]
                provider_id = provider_id_el.text
                provider_id_type = provider_id_el.get('schemeID')
                attach_evidence_provider(response_el, provider_id, provider_id_type, collection=True,
                                         agent_classification='EP')

                requester_id_el = regrep_request_el.xpath(
                    '//rim:Slot[@name="EvidenceRequester"]//sdg:Agent[sdg:Classification/text()="ER"]/sdg:Identifier',
                    namespaces=NSMAP
                )[0]
                requester_id = requester_id_el.text
                requester_id_type = requester_id_el.get('schemeID')
                attach_evidence_requester(response_el, requester_id, requester_id_type, collection=False,
                                          agent_classification=None)

                result_list = lxml.etree.SubElement(
                    response_el, rim_ns('RegistryObjectList')
                )
                #response_payload_parts = self.call_base_registry(
                #    regrep_request_el, response_el,requestid, requested_evidence_type, provider_id
                #)
                # The response is a list of payload parts: the QueryResponse document and any evidences parts.
                # We call the RegRep layer to process the request and create the payload parts for us.
                # Then we attach each of the payloads in order with their content identifier and content type.
                #response_payload_parts = self.create_regrep_response(
                #    regrep_request_el,
                #    edelivery_request.c1_party_id, edelivery_request.c1_party_id_type,
                #    edelivery_request.c4_party_id, edelivery_request.c4_party_id_type
                #)

        print("Created response: {}".format(lxml.etree.tostring(response_el)), flush=True)

        if edelivery_response.action == 'ExceptionResponse':
            (validation_failures_count, validation_report) = self.validate_query_exception(
                response_el, lxml.etree.tostring(response_el)
            )
            print('Exception validation: {} errors \n {}'.format(validation_failures_count, validation_report))
        else:
            (validation_failures_count, validation_report) = self.validate_response(
                response_el, lxml.etree.tostring(response_el)
            )
            print('Response validation: {} errors \n {}'.format(validation_failures_count, validation_report))

        edelivery_response.attach(
            "{}@oots.eu".format(uuid.uuid4()),
            'application/x-ebrs+xml',
            lxml.etree.tostring(response_el)
        )
        for (cid, mimetype, response_content) in response_payload_parts:
            edelivery_response.attach(cid, mimetype, response_content)

        # Now we can submit the response message to the Access Point
        result = oots.ex.domibus_ws.submit(
            edelivery_response,
            self.settings['accesspoint']['accesspointurl']
        )

    def validate_query_request(self, request_el, requestcontent):
        validation_failures_count = 0
        validation_report = ''
        # 1:  XSD
        if not self.driver.validate(request_el):
            validation_report = str(self.driver.error_log.last_error)+"\n"
            validation_failures_count = 1

        # 2:  Schematron
        for (validator, label) in [
            (self.edm_req_c, 'EDM-REQ-C.sch'), (self.edm_req_s, 'EDM-REQ-S.sch')]:
            valid = validator.isvalid(xml_text=requestcontent)
            (count, report) = validator.validate(xml_text=requestcontent)
            validation_failures_count += int(count)
            validation_report = validation_report + report

        return (validation_failures_count, validation_report)

    def validate_query_exception(self, response_el, responsecontent):
        validation_failures_count = 0
        validation_report = ''
        # 1:  XSD
        if not self.driver.validate(response_el):
            validation_report = str(self.driver.error_log.last_error)+"\n"
            validation_failures_count = 1

        # 2:  Schematron
        for (validator, label) in [
            (self.edm_err_c, 'EDM-ERR-C.sch'), (self.edm_err_s, 'EDM-ERR-S.sch')]:
            valid = validator.isvalid(xml_text=responsecontent)
            (count, report) = validator.validate(xml_text=responsecontent)
            validation_failures_count += int(count)
            validation_report = validation_report + report

        return (validation_failures_count, validation_report)

    def validate_response(self, response_el, responsecontent):
        validation_failures_count = 0
        validation_report = ''
        # 1:  XSD
        if not self.driver.validate(response_el):
            validation_report = str(self.driver.error_log.last_error)+"\n"
            validation_failures_count = 1

        # 2:  Schematron
        for (validator, label) in [
            (self.edm_resp_c, 'EDM-RESP-C.sch'), (self.edm_resp_s, 'EDM-RESP-S.sch')]:
            valid = validator.isvalid(xml_text=responsecontent)
            (count, report) = validator.validate(xml_text=responsecontent)
            validation_failures_count += int(count)
            validation_report = validation_report + report

        if validation_failures_count > 0:
            print("Validation report: \n{}".format(validation_report))
        return (validation_failures_count, validation_report)

    def get_requested_evidence_type(self, request_el):
        return(request_el.findtext(
            './/{}/{}'.format(
                sdg_ns('DataServiceEvidenceType'),
                sdg_ns('Identifier')
            )
        ))

    def init_regrep_response(self, messageid, request_el):
        response_el = lxml.etree.Element(
            query_ns('QueryResponse'),
            nsmap=NSMAP
        )
        if request_el is not None:
            # We set the requestId attribute on the response with as value the value of the id attribute
            # of the request.  This allows correlation of requests and responses by the requester.
            requestid = request_el.get('id')
            if requestid is not None:
                response_el.set('requestId', requestid)

        #Specification identifier slot
        attach_simple_slot(
            response_el, 'SpecificationIdentifier', 'rim:StringValueType', 'oots-edm:v1.0'
        )

        #EvidenceResponseIdentifier slot
        attach_simple_slot(
            response_el, 'EvidenceResponseIdentifier', 'rim:StringValueType', str(uuid.uuid4())
        )

        #Issue DateTime slot
        attach_simple_slot(
            response_el, 'IssueDateTime', 'rim:DateTimeValueType', datetime.datetime.now().isoformat()
        )
        return response_el

    def create_exception(self, messageid, request_el, message=None, detail=None,
                         severity='urn:oasis:names:tc:ebxml-regrep:ErrorSeverityType:Error',
                         type='query:QueryExceptionType',
                         code='EDM:ERR:0008	'):
        exception_el = lxml.etree.Element(
            rs_ns('Exception')
        )
        for (attribute, value) in [
            ('message', message),
            ('detail', detail),
            ('severity', severity),
            ('code', code)
        ]:
            if value is not None:
                exception_el.set(attribute, value)
        exception_el.set(xsi_ns('type'),type)
        attach_simple_slot(
            exception_el, 'Timestamp', 'rim:DateTimeValueType', datetime.datetime.now().isoformat()
        )
        return exception_el

    def call_base_registry(self, request_el, response_el, requestid,
                           requested_evidence_type, requester_id):
        return [("{}@oots.eu".format(uuid.uuid4()),
                 'application/x-ebrs+xml',
                 lxml.etree.tostring(response_el))]

    def process_requests(self, interval=20):
        # To operate a data service as a server,  we periodically check if there are requests we need
        # to reply to.  This sample code use an interval of 20 seconds, which is OK for demonstrations and
        # development,  but obviously too long for interactive use.
        print('Checking for evidence requests ..\n', flush=True)
        while True:
            try:
                oots.ex.domibus_ws.process_messages(
                    self.settings['accesspoint']['accesspointurl'],
                    self.handle_edelivery_request
                )
            except Exception:
                print("-" * 60)
                traceback.print_exc(file=sys.stdout)
                print("-" * 60)
            gevent.sleep(interval)

    def serve_forever(self):
        (keyfile, certfile) = oots.utils.sslcontext.generate_sslcontext()
        http_server = WSGIServer(('', self.settings['webserver']['server_port']), self, keyfile=keyfile, certfile=certfile)
        gevent.joinall([
            gevent.spawn(http_server.serve_forever),
            gevent.spawn(self.process_requests)
        ])
