"""
Domibus Web Service Interface
"""

import requests, base64, lxml.etree

_NSMAP = {
    'query': 'urn:oasis:names:tc:ebxml-regrep:xsd:query:4.0',
    'rs': 'urn:oasis:names:tc:ebxml-regrep:xsd:rs:4.0',
    'rim': 'urn:oasis:names:tc:ebxml-regrep:xsd:rim:4.0',
    'xsi': 'http://www.w3.org/2001/XMLSchema-instance',
    'sdg': 'http://data.europa.eu/sdg#',
    's12':'http://www.w3.org/2003/05/soap-envelope',
    'backend':'http://org.ecodex.backend/1_1/',
    'eb3':'http://docs.oasis-open.org/ebxml-msg/ebms/v3.0/ns/core/200704/'
}

def s12_ns(el):
    return '{{{}}}{}'.format(_NSMAP['s12'],el)

def eb3_ns(el):
    return '{{{}}}{}'.format(_NSMAP['eb3'],el)

def backend_ns(el):
    return '{{{}}}{}'.format(_NSMAP['backend'],el)

class eDeliveryMessage():
    def __init__(self,
                 c1_party_id=None, c1_party_id_type=None,
                 c2_party_id=None, c2_party_id_type=None,
                 c3_party_id=None, c3_party_id_type=None,
                 c4_party_id=None, c4_party_id_type=None,
                 service="http://docs.oasis-open.org/ebxml-msg/as4/200902/service",
                 serviceType="urn:oasis:names:tc:ebcore:ebrs:ebms:binding:1.0",
                 action="http://docs.oasis-open.org/ebxml-msg/as4/200902/action",
                 conversationid=None,
                 properties={},
                 soapenvelope=None):
        self.messageid=None
        self.c1_party_id=c1_party_id
        self.c1_party_id_type=c1_party_id_type
        self.c2_party_id=c2_party_id
        self.c2_party_id_type=c2_party_id_type
        self.c3_party_id=c3_party_id
        self.c3_party_id_type=c3_party_id_type
        self.c4_party_id=c4_party_id
        self.c4_party_id_type=c4_party_id_type
        self.service=service
        self.serviceType=serviceType
        self.action = action
        self.conversationid = conversationid
        self.properties = properties
        self.payloads = []
        self.properties = properties
        self.soapenvelope = soapenvelope

        # C2 and C3 are mandatory.  In four-corner exchanges, also set C1 and C4
        if c2_party_id is None or c3_party_id is None:
            raise Exception('Set value for c2 and c3 party id')

    def attach(self, contentid, contenttype, value):
        self.payloads.append( (
            contentid, contenttype, value
        )
    )

    def get_payload_by_position(self, position):
        return self.payloads[position]

    def get_payload_by_contentid(self, requested_contentid):
        for (contentid, contenttype, value) in self.payloads:
            if requested_contentid == contentid:
                return (contentid, contenttype, value)
        raise Exception('No payload part with id {} in message'.format(requested_contentid))

    def toxml(self):
        envelope_el = lxml.etree.Element(s12_ns('Envelope'), nsmap=_NSMAP)
        header_el = lxml.etree.SubElement(envelope_el, s12_ns('Header'))
        messaging_el = lxml.etree.SubElement(header_el, eb3_ns('Messaging'))
        usermessage_el = lxml.etree.SubElement(messaging_el, eb3_ns('UserMessage'))
        partyinfo_el = lxml.etree.SubElement(usermessage_el, eb3_ns('PartyInfo'))
        from_party_el = lxml.etree.SubElement(partyinfo_el, eb3_ns('From'))
        from_partyid_el = lxml.etree.SubElement(from_party_el, eb3_ns('PartyId'))
        if self.c2_party_id_type is not None:
            from_partyid_el.set('type', self.c2_party_id_type)
        from_partyid_el.text = self.c2_party_id
        from_role_el = lxml.etree.SubElement(from_party_el, eb3_ns('Role'))
        from_role_el.text = 'http://sdg.europa.eu/edelivery/gateway'
        to_party_el = lxml.etree.SubElement(partyinfo_el, eb3_ns('To'))
        to_partyid_el = lxml.etree.SubElement(to_party_el, eb3_ns('PartyId'))
        to_partyid_el.text = self.c3_party_id
        if self.c3_party_id_type is not None:
            to_partyid_el.set('type', self.c3_party_id_type)
        to_role_el = lxml.etree.SubElement(to_party_el, eb3_ns('Role'))
        to_role_el.text = 'http://sdg.europa.eu/edelivery/gateway'
        collaborationinfo_el = lxml.etree.SubElement(usermessage_el, eb3_ns('CollaborationInfo'))
        service_el = lxml.etree.SubElement(collaborationinfo_el, eb3_ns('Service'))
        service_el.set('type', self.serviceType)
        service_el.text = self.service
        action_el = lxml.etree.SubElement(collaborationinfo_el, eb3_ns('Action'))
        action_el.text = self.action
        if self.conversationid is not None:
            conversationid_el = lxml.etree.SubElement(collaborationinfo_el, eb3_ns('ConversationId'))
            conversationid_el.text = self.conversationid
        if self.c1_party_id is not None or self.c4_party_id is not None:
            # Create Message Properties element if C1 and/or C4 and/or any attributes are specified.
            properties_el = lxml.etree.SubElement(usermessage_el, eb3_ns('MessageProperties'))
            if self.c1_party_id is not None:
                c1_property_el = lxml.etree.SubElement(
                    properties_el, eb3_ns('Property'), name="originalSender"
                )
                c1_property_el.text = self.c1_party_id
                if self.c1_party_id_type is not None:
                    c1_property_el.set('type', self.c1_party_id_type)
            if self.c4_party_id is not None:
                c4_property_el = lxml.etree.SubElement(
                    properties_el, eb3_ns('Property'), name="finalRecipient"
                )
                c4_property_el.text = self.c4_party_id
                if self.c4_party_id_type is not None:
                    c4_property_el.set('type', self.c4_party_id_type)
            for (property, property_value) in self.properties.items():
                prop_el = lxml.etree.SubElement(
                    properties_el, eb3_ns(property)
                )
                prop_el.text=property_value
        payloadinfo_el= lxml.etree.SubElement(usermessage_el, eb3_ns('PayloadInfo'))
        body_el= lxml.etree.SubElement(envelope_el, s12_ns('Body'))
        submit_el= lxml.etree.SubElement(body_el, backend_ns('submitRequest'))
        for (contentid, contentype, value) in self.payloads:
            partinfo_el = lxml.etree.SubElement(payloadinfo_el, eb3_ns('PartInfo'))
            partinfo_el.set('href', _cid(contentid))
            partproperties_el = lxml.etree.SubElement(partinfo_el, eb3_ns('PartProperties'))
            partproperty_el = lxml.etree.SubElement(partproperties_el, eb3_ns('Property'))
            partproperty_el.set('name', 'MimeType')
            partproperty_el.text = contentype
            payload_el = lxml.etree.SubElement(submit_el, 'payload')
            payload_el.set('payloadId', _cid(contentid))
            payload_el.set('contentType', contentype)
            payloadvalue_el = lxml.etree.SubElement(payload_el, 'value')
            payloadvalue_el.text = base64.b64encode(value)
        return(envelope_el)

    def tostring(self):
        soapenv_el = self.toxml()
        return(lxml.etree.tostring(soapenv_el))

def submit(message, backend_url):
    headers = {'content-type': 'application/soap+xml'}
    body = message.tostring()
    return requests.post(backend_url, data=body, headers=headers, timeout=30)

def list_pending_messages(backend_url):
    headers = {'content-type': 'application/soap+xml'}
    envelope_el = lxml.etree.Element(s12_ns('Envelope'), nsmap=_NSMAP)
    header_el = lxml.etree.SubElement(envelope_el, s12_ns('Header'))
    body_el = lxml.etree.SubElement(envelope_el, s12_ns('Body'))
    listrequest_el = lxml.etree.SubElement(
        body_el, backend_ns('listPendingMessagesRequest')
    )
    body = lxml.etree.tostring(envelope_el, pretty_print=True)
    response=requests.post(backend_url, data=body, headers=headers, timeout=30)
    listresponse_el = lxml.etree.fromstring(response.content)
    messageids = listresponse_el.findall('.//messageID')
    return([i.text for i in messageids])

def retrieve_message(backend_url, messageid, handler):
    headers = {'content-type': 'application/soap+xml'}
    envelope_el = lxml.etree.Element(s12_ns('Envelope'), nsmap=_NSMAP)
    header_el = lxml.etree.SubElement(envelope_el, s12_ns('Header'))
    body_el = lxml.etree.SubElement(envelope_el, s12_ns('Body'))
    retrieverequest_el = lxml.etree.SubElement(
        body_el, backend_ns('retrieveMessageRequest')
    )
    messageid_el = lxml.etree.SubElement(retrieverequest_el, 'messageID')
    messageid_el.text= messageid
    body = lxml.etree.tostring(envelope_el, pretty_print=True)
    response=requests.post(backend_url, data=body, headers=headers, timeout=30)
    message= load_content(messageid, response.content)
    handler(messageid, message)

def load_content(messageid, content):
    tree=lxml.etree.fromstring(content)
    frompartyid_el= tree.find(".//{}/{}".format(eb3_ns('From'),eb3_ns('PartyId')))
    frompartyid=frompartyid_el.text
    frompartyidtype= frompartyid_el.get('type',None)
    topartyid_el= tree.find(".//{}/{}".format(eb3_ns('To'),eb3_ns('PartyId')))
    topartyid=topartyid_el.text
    topartyidtype= topartyid_el.get('type', None)
    service_el= tree.find(".//{}".format(eb3_ns('Service')))
    service = service_el.text
    service_type = service_el.get('type', None)
    action= tree.find(".//{}".format(eb3_ns('Action'))).text
    conversationid = tree.find(".//{}".format(eb3_ns('ConversationId'))).text
    for property in tree.findall(".//{}".format(eb3_ns('Property'))):
        if property.get('name') == 'originalSender':
            c1_party_id=property.text
            c1_party_id_type=property.get('type',None)
        if property.get('name') == 'finalRecipient':
            c4_party_id = property.text
            c4_party_id_type = property.get('type', None)

    message = eDeliveryMessage(
        c1_party_id=c1_party_id, c1_party_id_type=c1_party_id_type,
        c2_party_id=frompartyid, c2_party_id_type=frompartyidtype,
        c3_party_id=topartyid, c3_party_id_type=topartyidtype,
        c4_party_id=c4_party_id, c4_party_id_type=c4_party_id_type,
        service=service,  serviceType=service_type, action=action,
        conversationid=conversationid,
        soapenvelope=tree
    )
    payload_els = tree.findall('.//payload')
    partinfo_els = tree.findall('.//{}'.format(eb3_ns('PartInfo')))
    for payloadel, partinfoel in zip(payload_els, partinfo_els):
        content_id = partinfoel.get('href')
        content_type = partinfoel.findtext('.//{}'.format(eb3_ns('Property')))
        content_value = payloadel.findtext('value')
        message.attach(_strip_cid(content_id), content_type, base64.b64decode(content_value))
    message.messageid=messageid
    return message

def default_handler(messageid, message):
    print("Default handler on {}:\n{}".format(
        messageid,
        lxml.etree.tostring(message.soapenvelope)
    ))

def process_messages(backend_url, handler=default_handler):
    messageids = list_pending_messages(backend_url)
    print("{} pending messages".format(len(messageids)))
    for (count, messageid) in enumerate(messageids, start=1):
        print('{}: {}'.format(count, messageid))
        retrieve_message(backend_url, messageid, handler)

def _cid(identifier):
    return "cid:{}".format(identifier)

def _strip_cid(identifier):
    if identifier[:4] != 'cid:':
        raise Exception('Identifier {},  expecting it to start with _cid:_')
    return identifier[4:]

def _text(element):
    return element.text

