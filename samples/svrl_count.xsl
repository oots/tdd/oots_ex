<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
    version="2.0">
    <xsl:output  media-type="text" omit-xml-declaration="yes"/>
    <xsl:template  match="svrl:schematron-output">
        <xsl:value-of select="count(child::svrl:failed-assert[not(@role) or @role = 'fatal' 
            or @role = 'FATAL' or @role='error' or @role='ERROR'])"/>     
    </xsl:template>
</xsl:stylesheet>