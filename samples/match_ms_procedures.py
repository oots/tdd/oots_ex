
from oots.cs.eb import get_list_of_requirements, get_evidence_types
from oots.cs.dsd import get_data_services
from oots.cs.cs_common import is_exception
import sys, lxml, logging

logging.basicConfig(level=logging.INFO)

def match_ms_procedures(ms_a, ms_b, procedure_id, environment):
    r = get_list_of_requirements(ms_a, procedure_id, environment)
    requirements = r.xpath(
        '//oots:Requirement/oots:Identifier/text()',
        namespaces={'oots':'http://data.europa.eu/p4s'}
    )
    (failure, exception) = is_exception(r)
    if failure:
        logging.error('Exception getting requirements for {}: {}'.format(
            procedure_id, exception
        ))
    else:
        for (requirement_no, requirement_id) in enumerate(
                requirements, start=1
        ):
            logging.info('Requirement {} for procedure {} in {}: {}'.format(
                requirement_no, procedure_id, ms_a, requirement_id
            ))
            r2 = get_evidence_types(
                    ms_b, requirement_id, environment
            )
            (failure, exception) = is_exception(r2)
            if failure:
                logging.error('Exception getting evidence types for {}: {}'.format(
                    requirement_id, exception
                ))
            else:
                for (classification_no, classification) in enumerate(
                    r2.xpath(
                        '//oots:EvidenceTypeClassification/text()',
                        namespaces={'oots': 'http://data.europa.eu/p4s'}
                    ),
                    start=1
                ):
                    logging.info('Requirement {} in {} classification {}: {}'.format(
                        requirement_no,  ms_b, classification_no, classification
                    ))
                    r3 = get_data_services(ms_b, classification, environment)
                    (failure, exception) = is_exception(r3)
                    if failure:
                        logging.error('Exception getting data services for {}: {}'.format(
                            classification, exception
                        ))
                    else:
                        for dset_id in r3.xpath(
                            '//oots:DataServiceEvidenceType/oots:Identifier/text()',
                            namespaces={'oots': 'http://data.europa.eu/p4s'}
                        ):
                            logging.info('Classification {} {} data service evidence type id: {}'.format(
                                classification_no, classification, dset_id
                            ))
                            """ 
                            for access_service in r3.xpath(
                                '//oots:AccessService',
                                namespaces={'oots': 'http://data.europa.eu/p4s'}
                            ):
                                (c3_value, c3_scheme) = _get_party(access_service)
                                print('c3_value {} c3_scheme {}'.format(c3_value, c3_scheme))
                                c4s = []
                                for publisher in access_service.xpath(
                                    'child::oots:Publisher',
                                    namespaces={'oots': 'http://data.europa.eu/p4s'}
                                ):
                                    (c4_value, c4_scheme) = _get_party(publisher)
                                    print('c4 value {} c4 scheme {}'.format(c4_value, c4_scheme))
                                    c4s.append(_get_party(publisher))
                            """


def _get_party(access_service):
    identifier = access_service.xpath(
        'child::oots:Identifier',
        namespaces={'oots': 'http://data.europa.eu/p4s'}
    )[0]
    return (identifier.text, identifier.get('schemeID'))

if __name__ == '__main__':
    if len(sys.argv) < 3:
        raise(Exception('match_ms_procedures(ms_a, ms_b, procedure_id, environment)'))
    match_ms_procedures(sys.argv[1], sys.argv[2], sys.argv[3], sys.argv[4])




