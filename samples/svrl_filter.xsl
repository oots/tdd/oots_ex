<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
    xmlns:sch="http://purl.oclc.org/dsdl/schematron"
    xmlns:error="https://doi.org/10.5281/zenodo.1495494#error"
    xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
    xmlns:schxslt-api="https://doi.org/10.5281/zenodo.1495494#api"
    xmlns:schxslt="https://doi.org/10.5281/zenodo.1495494"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:sdg="http://data.europa.eu/p4s"
    xmlns:rs="urn:oasis:names:tc:ebxml-regrep:xsd:rs:4.0"
    xmlns:rim="urn:oasis:names:tc:ebxml-regrep:xsd:rim:4.0"
    xmlns:query="urn:oasis:names:tc:ebxml-regrep:xsd:query:4.0"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xmlns:xlink="http://www.w3.org/1999/xlink"
    exclude-result-prefixes="xs error rdf schxslt-api schxslt"
    version="2.0">
    
    <xsl:output indent="yes"/>

    <xsl:template  match="svrl:schematron-output">
        <svrl:schematron-output>
            <xsl:apply-templates  select="child::svrl:failed-assert[not(@role) or @role = 'fatal' 
                or @role = 'FATAL' or @role='error' or @role='ERROR']"/>
        </svrl:schematron-output>
       <xsl:text>AAP: </xsl:text>
        <xsl:value-of select="count(child::svrl:failed-assert[not(@role) or @role = 'fatal' 
            or @role = 'FATAL' or @role='error' or @role='ERROR'])"/>     
    </xsl:template>
    
    <xsl:template match="svrl:failed-assert">
        <xsl:copy-of select="current()" />
    </xsl:template>
    
</xsl:stylesheet>