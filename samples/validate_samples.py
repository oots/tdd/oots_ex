
"""
To run this sample, install schematron.py somewhere in Python's path.
"""

from inspect import getsourcefile
from os.path import abspath, dirname, join
thisdir = dirname(abspath(getsourcefile(lambda: 0)))

import schematron



schemas = [ 'https://code.europa.eu/oots/tdd/tdd_chapters/-/raw/master/OOTS-EDM/sch/EDM-REQ-C.sch',
            'https://code.europa.eu/oots/tdd/tdd_chapters/-/raw/master/OOTS-EDM/sch/EDM-REQ-S.sch'
            ]

instances = [
    "invalid_request.xml"
]


for schema in schemas:
    validator = schematron.SchematronValidator(xml_uri=schema)
    for instance in instances:
        sourcefile = join(thisdir,instance)
        valid = validator.isvalid(xml_file_name=sourcefile)
        (count, report) = validator.validate(xml_file_name=sourcefile)
        print("Vality {} for {} ({} fatal/error) for {}\n{}".format(valid, instance, count, schema, report))


