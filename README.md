# OOTS evidence exchange

## Overview

This library provides simple client and server functionality to send and 
receive evidence requests and responses using the mechanisms 
defined in the design documents of the 
[Once-Only Technical System (OOTS)](https://ec.europa.eu/digital-building-blocks/wikis/display/OOTS/OOTSHUB+Home)
 developed for the EU's [Single Digital Gateway regulation](https://ec.europa.eu/growth/single-market/single-digital-gateway_en). 

Features: 
- Implements the [QueryManager interface](http://docs.oasis-open.org/regrep/regrep-core/v4.0/os/regrep-core-rs-v4.0-os.html#__RefHeading__32245_422331532) of the [OASIS RegRep4](http://docs.oasis-open.org/regrep/regrep-core/v4.0/os/regrep-core-rs-v4.0-os.html) as 
profiled for [OOTS evidence exchange](https://ec.europa.eu/digital-building-blocks/wikis/display/TDD/Chapter+4%3A+Evidence+Exchange+-+Q1+2023)- .  
- Provide classes for Data Service clients and servers. 
- Use [eDelivery](https://ec.europa.eu/digital-building-blocks/wikis/display/DIGITAL/eDelivery) by supporting the backend interface for [Domibus](https://ec.europa.eu/digital-building-blocks/wikis/display/DIGITAL/Domibus) Access Points for submitting and retrieving 
eDelivery messages. The server periodically polls an Access Point for inbound requests and submits positive (responses) 
or negative (exceptions) responses. 
- Raise and return exceptions relating to validation of:
  - Requests that are not well-formed requests.
  - Requests that are not valid against the SDG and RegRep4 XML Schemas.
  - Requests that trigger assertion failures (fatal or error) for SDG Schematron definitions.

Currently unsupported:
- Register your own handler to invoke your backend code to process requests. For now,  the response 
  to a valid requests is simply an empty set. 
 - Support for [Evidence Preview](https://ec.europa.eu/digital-building-blocks/wikis/display/TDD/4.9+-+Evidence+Preview+-+Q1+2023), i.e. 
-interaction of the Data Service with a Preview Space.  The data service server does already include an HTTP 
server, what is missing is support for preview URLs.
- Timeouts.
- Intermediate Platform.

## Installation and configuration

This library was developed using CPython 3.11.  It uses current best practices using *pip*,
*setuptools* and configuration using *pyproject.toml*.

To build the library,  you can use the *build* utility.  You can install it as follows:

   > pip install --upgrade build
   
Then you can build as follows:

   > python -m build

This will build a wheel that you can install (typically in a virtual environment) using *pip*:

   > pip install oots-0.0.1-py3-none-any.whl

This will also download and install the dependencies.

After this,  the library is in your path and you can, for example, start developing a dataservice.

   > from oots.ex import dataservice

## Evidence exchange

The OOTS library has a module *oots.ex* for evidence exchange.

### Domibus

The *domibus_ws* library can be used to connect to a running Domibus server to send and receive eDelivery messages.

### Dataservice

The *dataservice.py" library support development of Data Service clients and servers.

## Utilities

### Schematron

The *schematron.py* library (built on *saxonche*) allows validation of XML samples based on 
Schematron definitions that rely on XSLT 2.

### Discovery

The *cs_discovery.py* library allows discovering OOTS Common Services that use the DNS-based discovery feature
defined in section 3.4 of the OOTS TDDs.

### SSL context

The *sslcontext.py" library generates a private ECDSA key and self-signed certificate for use with *gevent* and *Flask*. 
It is used by the dataservice library.

### Cross validation

The *crossvalidate.py* library supports Schematron 
files that express constraints across different XML artifacts.
It runs a simple Web server that support uploading 
the artifacts individually and returns the result.



## Samples and misc other

To be uploaded.

## License

This software is licensed under [European Union Public 
Licence (EUPL) version 1.2](https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12).

This library uses [schxsjt](https://github.com/schxslt/schxslt), an XSLT based Schematron
processor that is copyright (c) 2018-2021 David Maus and is licensed under [MIT 
license](https://github.com/schxslt/schxslt/blob/master/LICENSE). 

## Previous work

This repository is hosted at 
[code.europa.eu](https://code.europa.eu/info/about/-/blob/master/README.md), 
the current future-proof solution for hosting open source projects of 
the European Commission. It aims to incorporate and update the existing 
[oots_ex](https://ec.europa.eu/digital-building-blocks/code/projects/OOP/repos/oots_ex/browse)
repository. Once all features are provided, that repository will be closed.

## Support

This library is unsupported and provided as-is without warranty. 

